package herencia;

import producto.ProductoCongelado;
import producto.ProductoFresco;
import producto.ProductoRefrigerado;

public class Supermercado {
	public static void main(String[] args) {
		ProductoFresco productoFresco = new ProductoFresco("05-05-2025", 123456, new java.util.Date(), "España");
		ProductoRefrigerado productoRefrigerado = new ProductoRefrigerado("05-05-2025", 789012, "ABC123");
		ProductoCongelado productoCongelado = new ProductoCongelado("05-05-2025", 345678, -18);
		
		System.out.println("Producto Fresco:\n" + productoFresco.toString() + "\n");
		System.out.println("--------------------------------------------------------------");
		System.out.println("Producto Refrigerado:\n" + productoRefrigerado.toString() + "\n");
		System.out.println("--------------------------------------------------------------");
		System.out.println("Producto Congelado:\n" + productoCongelado.toString());
	}
}
