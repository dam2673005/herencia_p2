package producto;

public abstract class Producto {
    private String caducidad;
    private int numeroLote;
    
    public Producto(String caducidad, int numeroLote) {
        this.caducidad = caducidad;
        this.numeroLote = numeroLote;
    }
    
    public String getCaducidad() {
        return caducidad;
    }
    
    public void setCaducidad(String caducidad) {
        this.caducidad = caducidad;
    }
    
    public int getNumeroLote() {
        return numeroLote;
    }
    
    public void setNumeroLote(int numeroLote) {
        this.numeroLote = numeroLote;
    }
    
    @Override
    public String toString() {
        return "\nCaducidad: " + getCaducidad() + "\nNúmero de Lote: " + getNumeroLote();
    }
}
