package producto;

public class ProductoCongelado extends Producto{
	private int tempRecomendada;
	
	public ProductoCongelado(String caducidad, int numeroLote, int tempRecomendada) {
		super(caducidad, numeroLote);
		this.tempRecomendada = tempRecomendada;
	}
	
	public int getTempRecomendada() {
		return tempRecomendada;
	}
	
	public void setTempRecomendada(int tempRecomendada) {
		this.tempRecomendada = tempRecomendada;
	}
	
	@Override
    public String toString() {
        return super.toString() + "\nTemperatura Recomendada: " + tempRecomendada + " °C";
    }
}
