package producto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductoFresco extends Producto {
    private Date fechaEnvasado;
    private String paisOrigen;

    public ProductoFresco(String caducidad, int numeroLote, Date fechaEnvasado, String paisOrigen) {
        super(caducidad, numeroLote);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
    }

    public Date getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(Date fechaEnvasado) {
        this.fechaEnvasado = fechaEnvasado;
    }
	
	public String getPaisOrigen() {
		return paisOrigen;
	}
	
	public void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	
	@Override
	public String toString() {
	    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	    return super.toString() +
	           "\nFecha de Envasado: " + format.format(fechaEnvasado) + "\nPaís de Origen: " + paisOrigen;
	}
}
