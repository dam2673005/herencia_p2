package producto;

public class ProductoRefrigerado extends Producto {
    private String codOrganismo;

    public ProductoRefrigerado(String caducidad, int numeroLote, String codOrganismo) {
        super(caducidad, numeroLote);
        this.codOrganismo = codOrganismo;
    }

    public String getCodOrganismo() {
        return codOrganismo;
    }

    public void setCodOrganismo(String codOrganismo) {
        this.codOrganismo = codOrganismo;
    }

    @Override
    public String toString() {
        return super.toString() +
               "\nCódigo de Organismo: " + codOrganismo;
    }
}
